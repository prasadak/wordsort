#ifndef __HEADER_H__
#define __HEADER_H__

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <stdint.h>

//#define DEBUG_PR		/*!< Debug print enable macro */
//#define PRINT_ENABLE		/*!< Message print enable macro */

#ifdef DEBUG_PR
	#define dbg_msg(...) do { printf("\nDEBUG: " __VA_ARGS__); } while(0)
#else
	#define dbg_msg(...) do { } while(0)
#endif	/*! DEBUG_PR */

#ifdef PRINT_ENABLE
	#define pr_msg(...) do { printf(__VA_ARGS__); } while (0)
#else
	#define pr_msg(...) do { } while (0)
#endif	/*! PRINT_ENABLE */

#define ERR_BUF_SIZE	256

#define err_msg(...) \
	do { \
	char err_buf[ERR_BUF_SIZE]; \
	\
	fprintf(stderr, "\nERROR: in file: %s\t in function: %s\t " \
			"@line number: %d\n", __FILE__, __func__, __LINE__); \
	sprintf(err_buf, __VA_ARGS__); \
	perror(err_buf);\
	} while (0)

#endif /*! __HEADER_H__ */
