.PHONY : clean
VPATH = ./source ./header ./object

SRC := $(shell ls source/)
OBJ = $(SRC:.c=.o)
CC = gcc
FLAG = -g -Wstrict-prototypes -o

wordSort : $(OBJ) header.h
	$(CC) $(FLAG) $@ $^
	mkdir -p object
	-mv *.o object
	@echo Make file executed

%.o : %.c
	$(CC) -c $^ -I header

clean :
	-rm -rf ./object
	-rm -f wordSort
