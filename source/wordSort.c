#include "header.h"
#define MAX_WORD_LEN 64
#define TOP_WORDS_COUNT 100

/*! Structure for storing a data in the Binary tree */
struct tree_node {
	struct tree_node *left;
	struct tree_node *right;
	int32_t word_count;
	char word[];
};
typedef struct tree_node NODE;

/*! Hash list structure node for storing most repeated words */
struct hashList_arr_node {
	int word_count;
	char word[MAX_WORD_LEN];
};

static uint32_t total_words_count; /*! total number of words */
static uint32_t diff_words_count; /*! different words count */
static NODE *tree_root_ptr; /*! Binary tree root pointer */
static struct hashList_arr_node words_hashList[TOP_WORDS_COUNT]; /* static array for top repeated words */

ssize_t getdelim(char **linep, size_t *n, int delim, FILE *fp)
{
	int ch;
	size_t i = 0;

	if (!linep || !n || !fp) {
		//errno = EINVAL;
		return -1;
	}

	if (*linep == NULL) {
		if (NULL == (*linep = malloc(*n=128))) {
			*n = 0;
			//errno = ENOMEM;
			return -1;
		}
	}

	while ((ch = fgetc(fp)) != EOF) {
		if (i + 1 >= *n) {
			char *temp = realloc(*linep, *n + 128);
			if (!temp) {
				//errno = ENOMEM;
				return -1;
			}
			*n += 128;
			*linep = temp;
		}
		(*linep)[i++] = ch;
		if (ch == delim)
			break;
	}

	(*linep)[i] = '\0';
	return !i && ch == EOF ? -1 : i;
}

ssize_t getline_own(char **linep, size_t *n, FILE *fp)
{
	return getdelim(linep, n, '\n', fp);
}

/*! Free the memory which was allocated for binary tree */
void delete_words_Tree(NODE *node)
{
    if (node == NULL)
	    return;

    /* first delete both subtrees */
    delete_words_Tree(node->left);
    delete_words_Tree(node->right);

    /* then delete the node */
    dbg_msg("\nDeleting node: %s", node->word);
    free(node);
}

/*! Function to convert lower cases to upper cases */
void str_lo_to_up(char *str)
{
	while (*str) {
		if (*str >= 'a' && *str <= 'z')
			*str ^= 0x20;
		str++;
	}
}

#if 0
/*! Function to dispaly all the words in the binary tree */
void display_word_tree(NODE *root)
{
	if (!root) {
		pr_msg("\n\tTREE IS NOT PRESENT.\n");
		return;
	} else {
		if (root->left)
			display_word_tree(root->left);

		if (root->right)
			display_word_tree(root->right);
	}

	return;
}
#endif

/*! Function to sort the Hash list */
void sort_hashList(void)
{
	int i, j;

	for (i = 0; i < TOP_WORDS_COUNT; ++i) {
		for (j = (i + 1); j < TOP_WORDS_COUNT; ++j) {
			if (words_hashList[i].word_count <
					words_hashList[j].word_count) {
				struct hashList_arr_node temp;
				temp = words_hashList[i];
				words_hashList[i] = words_hashList[j];
				words_hashList[j] = temp;
			}
		}
	}
}

/*! Function to print the Hash list */
void print_hashList(void)
{
	pr_msg("TOP %d most common words:\n", TOP_WORDS_COUNT);

	for (int i = 0; i < TOP_WORDS_COUNT; i++) {
		if (words_hashList[i].word_count)
			pr_msg("%4d. %u\t%s\n", i+1, words_hashList[i].word_count,
					words_hashList[i].word);
	}
}

/*! Function to add new word/token to Hash list */
void add_to_hash_list(NODE *tree)
{
	int i;

	for (i = 0; i < TOP_WORDS_COUNT; i++) {
		if (strcmp(tree->word, words_hashList[i].word) == 0) {
			words_hashList[i].word_count = tree->word_count;
			return;
		}
	}

	if (diff_words_count < TOP_WORDS_COUNT) {
		for (i = 0; i < TOP_WORDS_COUNT; i++) {
			if (words_hashList[i].word_count == 0) {
				words_hashList[i].word_count = tree->word_count;
				strcpy(words_hashList[i].word, tree->word);
				return;
			}
		}
	} else {
		int min_index = 0;
		int min_val = words_hashList[0].word_count;

		for (i = 0; i < TOP_WORDS_COUNT; i++) {
			if (min_val > words_hashList[i].word_count) {
				min_val = words_hashList[i].word_count;
				min_index = i;
			}
		}

		if (words_hashList[min_index].word_count < tree->word_count) {
			words_hashList[min_index].word_count = tree->word_count;
			strcpy(words_hashList[min_index].word, tree->word);
		}
	}

	return;
}

/*! Function to add new word/token to binary tree */
NODE *ins_word_in_tree(NODE *root, char *token)
{
	if (!root) {
		NODE *temp = NULL;
		temp = malloc(sizeof(NODE) + strlen(token));
		if (!temp) {
			pr_msg("Memory alloc fail\n");
			return NULL;
		}

		strcpy(temp->word, token);


		diff_words_count++;
		temp->word_count = 1;
		temp->left = temp->right = NULL;

		add_to_hash_list(temp);

		dbg_msg("Word added: %s\n", temp->word);
		return temp;

	} else if (strcmp(token, root->word) < 0) {
		root->left = ins_word_in_tree(root->left, token);
	} else if (strcmp(token, root->word) > 0) {
		root->right = ins_word_in_tree(root->right, token);
	} else {
		root->word_count++;
		add_to_hash_list(root);
	}

	return root;
}

void add_word(char *word)
{
	total_words_count++;
	tree_root_ptr = ins_word_in_tree(tree_root_ptr, word);
}

void string_parse(char *line)
{
	char token[MAX_WORD_LEN];
	char *tmp = token;

	str_lo_to_up(line);

	while (*line && *line != '\n') {
		memset((void*) token, 0, MAX_WORD_LEN);

		while ((*line >= 'A' && *line <= 'Z') || *line == '\'') {
			*tmp = *line;
			tmp++;
			line++;
		}

		if (tmp != token) {
			*tmp = '\0';
			add_word(token);
		}

		line++;
		tmp = token;
	}
}

void write_output_to_file(char *file)
{
	char fileName[MAX_WORD_LEN];
	char *tmp_ptr;
	FILE *fp;

	tmp_ptr = strrchr(file, '/');
	if (tmp_ptr) {
		tmp_ptr++;
		snprintf(fileName, MAX_WORD_LEN, "output_%s", tmp_ptr);
	} else {
		snprintf(fileName, MAX_WORD_LEN, "output_%s", file);
	}

	printf("Output file name : %s\n", fileName);
	fp = fopen(fileName, "w");
	if (!fp) {
		err_msg("%s", fileName);
		return;
	}

	fprintf(fp, "Total number of words = %u\n", total_words_count);
	fprintf(fp, "Number of different words = %u\n", diff_words_count);
	fprintf(fp, "The %u most common words:\n", TOP_WORDS_COUNT);
	fprintf(fp, "WORD\t\tNUMBER OF OCCURRENCES\n");

	for (int i = 0; i < TOP_WORDS_COUNT; i++) {
		if (words_hashList[i].word_count)
			if (strlen(words_hashList[i].word) >= 8)
				fprintf(fp, "%s\t%u\n",
				words_hashList[i].word,
				words_hashList[i].word_count);
			else
				fprintf(fp, "%s\t\t%u\n",
				words_hashList[i].word,
				words_hashList[i].word_count);
	}

	fclose(fp);
}

int32_t main(int32_t argc, char	*argv[])
{
	FILE *fp;
	char *line = NULL;
	size_t len = 0;
	int32_t read;

	if (argc < 2) {
		pr_msg("Usage: %s <INPUT FILE PATH>\n", argv[0]);
		return EXIT_FAILURE;
	}

	fp = fopen(argv[1], "r");
	if (!fp) {
		err_msg("%s", argv[1]);
		return EXIT_FAILURE;
	}


	while ((read = getline_own(&line, &len, fp)) != -1) {
		dbg_msg("Retrieved line of length %d: %s\n", read, line);
		string_parse(line);
	}

	fclose(fp);

	/*! display_word_tree(tree_root_ptr); */
	delete_words_Tree(tree_root_ptr);
	sort_hashList();
	write_output_to_file(argv[1]);
	pr_msg("\nTotal words:     %u", total_words_count);
	pr_msg("\nDifferent words: %u\n", diff_words_count);
	print_hashList();
	free(line);

	return 0;
}

