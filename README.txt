1. To create binary execute below command
$make
-> Binary 'wordSort' will be created

2. Steps to test binary
	$./wordSort <file_path>
-> Top words are stored in a file with name 'output_<file_name>'

3. To clean
$make clean
